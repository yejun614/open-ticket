
function open_loading_screen() {
	const html = `
	<div id="loading-bg"></div>
	<div id="loading">
		<span uk-spinner="ratio: 4.5"></span>
		<p id="page-title" class="uk-text-lead uk-text-primary">Load Resources,<br />please waiting for loading</p>
		<p class="uk-text-small">Hello, World!</p>
	</div>`;

	$('body').append(html);
}

function close_loading_screen() {
	$('#loading').remove();
	$('#loading-bg').remove();
}

function load_resources(path_list, callback) {
	const load = (num) => {
		if (num >= path_list.length) {
			// Close loading screen
			close_loading_screen();

			// CALLBACK
			callback();
			return;
		}

		const url = path_list[num];
		load_res(url, () => { load(num + 1) });
	};

	// Start load
	load(0)
}

function load_res(path, callback) {
	let resource = undefined;
	let type = path.split('.').pop();

	if (type == 'js') {
		resource = document.createElement('script');
		resource.type = "text/javascript";

	} else if (type == 'css') {
		resource = document.createElement('link');
		resource.rel = "stylesheet";
		resource.type = "text/css";
	}

	resource.onload = () => { callback() };
	resource.onerror = () => { console.log(`Resource Lead Error: ${path}`); callback() };

	$('head').append(resource);

	if (type == 'js') {
		resource.src = path;
	} else if (type == 'css') {
		resource.href = path;
	}
}
