

function add_seat () {}

function show_seat_canvas () {}

function hide_seat_canvas () {}

function add_option () {
	let option_name = $('#option-form select').val();
	let option_price = $(`#option-form select option[value="${option_name}"]`).attr('price');

	let html = `<tr><td><input class="uk-checkbox" type="checkbox"></td><td>${option_name}</td><td>1</td><td>${option_price}</td></tr>`;
	$('#option-table tbody').append(html);
}

function remove_options () {
	let el = $('#option-table tbody tr');

	for (let n=0; n<el.length; n++) {
		let check = $(el[n]).find('input[type="checkbox"]')[0].checked;

		if (check) {
			$(el[n]).remove();
		}
	}

	$('#option-table thead input[type="checkbox"]').checked = false;
}

function show_terms_view (title) {
	$('#black-bg').css('visibility', 'visible')
	$('#terms-view').css('visibility', 'visible');

	$('#terms-view .' + title).css('visibility', 'visible');
}

function accept_term (title) {
	$('#terms-table .' + title + ' input[type="hidden"]').val('true');
	$('#terms-table .' + title + ' button').removeClass('uk-button-default');
	$('#terms-table .' + title + ' button').addClass('uk-button-primary');

	hide_terms_view();
}

function hide_terms_view () {
	$('#black-bg').css('visibility', 'hidden')
	$('#terms-view').css('visibility', 'hidden');

	let terms = $('#terms-view>div');
	for (let n=0; n<terms.length; n++) {
		$(terms[n]).css('visibility', 'hidden');
	}
}

function save_data_local () {}

// REQUEST PURCHASE button
function request_purchase () {
	let data = {
	};

	$.ajax()
	.done()
	.fail();
}

// CANCLE button
function purchase_cancle () {
	let result = confirm('Do you want to cancel it?');

	if (result) {
		// Redirect to Index page
		window.location.href = "/";
	}
}
