
// Alert
function uk_alert (style, message) {
	let html = `<div class="${style} uk-margin" uk-alert><a class="uk-alert-close" uk-close></a><p class="uk-margin-right">${message}</p></div>`;

	$('#alert-area').prepend(html);
}

// Price Table
var option_count = 0;

function add_option () {
	let option_name = $('#option_name').val();
	let option_price = $('#option_price').val();

	if (option_name == '' || option_price == '') {
		alert('Value is blink');
		return;
	}

	let html = `<tr class="option_${option_count}"><td>${option_name}</td><td>${option_price}</td><td><a onclick="remove_option(${option_count});">DELETE</a></td></tr>`;
	$('#option_table tbody').append(html);

	let html2 = `<li class="option_${option_count}"><button class="uk-button uk-button-default uk-width-1-1" onclick="add_seat(${option_count})">${option_name}</button></li>`;
	$('#canvas-seat ul').append(html2);

	option_count += 1;

	$('#option_name').val('');
	$('#option_price').val('');
}

function remove_option (num) {
	let check = confirm("Delete option in Price Table\nbut do not delete in Seat Table and Canvas.\nDo you continue?");

	if (check) {
		$('#option_table .option_' + num).remove();
		$('#canvas-seat ul .option_' + num).remove();
	}
}

// Set Tags
function add_tag () {
	let tag_name = $('#item_tag_form input[type="text"]').val();
	$('#item_tags').append(`<span class="uk-label uk-margin-small-right">${tag_name}</span>`);

	// Remove Tag
	$('#item_tags .uk-label').click((event) => {
		let result = confirm('Do you want to remove the tag?');

		if (result) {
			$(event.target).remove();
		}

	});

	$('#item_tag_form input[type="text"]').val('');
}

// Extra Data
function add_extra_data () {
	const key = $('#extra_form input[name="key"]').val();
	const value = $('#extra_form input[name="value"]').val();

	$('#extra_table tbody').append(`<tr><td>${key}</td><td>${value}</td><td><a onclick="remove_extra_data('${key}');">DELETE</a></td></tr>>`);

	// Clear Text Input
	$('#extra_form input[name="key"]').val('');
	$('#extra_form input[name="value"]').val('');
}

function remove_extra_data (key) {
	const el = $('#extra_table tbody tr');

	for (let n=0; n<el.length; n++) {
		const tr = $(el[n]).children();

		if (tr[0].innerHTML == key) {
			$(el[n]).remove();
		}
	}
}

// Payment Method
var UserDirectRemit = {
	'holder': '',
	'bank': '',
	'number': ''
}

function select_payment () {
	const method = paymentSelector.getSelectedItem().value;

	const el_method = $('#payment-method .method')[0];
	const el_description = $('#payment-method .description')[0];
	const el_hidden = $('#payment-method input[name="method"]')[0];

	if (method == 'no-pay') {
		el_method.innerHTML = "No pay";
		el_description.innerHTML = "<p>Do not anything on purchase request.</p>";

		$(el_hidden).val('no-pay');

	} else if (method == 'UserDirectRemit') {
		el_method.innerHTML = "User Direct Money Transfer";
		el_description.innerHTML = "<p>When item purchase, money transfer to seller by user directly.</p>";

		$(el_hidden).val('UserDirectRemit');
	}
}

function setting_payment () {
	const method = $($('#payment-method input[name="method"]')[0]).val();

	if (method == 'UserDirectRemit') {
		UIkit.modal('#payment-setting').show();
	}
}

function clear_setting_payment () {
	$('#payment-setting input[name="holder"]').val('');
	$('#payment-setting input[name="bank"]').val('');
	$('#payment-setting input[name="number"]').val('');

	UIkit.modal('#payment-setting').hide();
}

function apply_setting_payment () {
	UserDirectRemit.holder = $('#payment-setting input[name="holder"]').val();
	UserDirectRemit.bank = $('#payment-setting input[name="bank"]').val();
	UserDirectRemit.number = $('#payment-setting input[name="number"]').val();

	clear_setting_payment();
}

// DELETE
function delete_data () {
	const itemID = $('#item-ID')[0].innerHTML;
	if (itemID == "") {
		uk_alert('uk-alert-danger', 'No Item ID, first SAVE the item');
		return;
	}

	UIkit.modal.confirm('Do you want to remove this item?').then(function () {
		$.ajax({
			url: '/manager/item/remove',
			method: "POST",
			data: JSON.stringify({'id': itemID}),
			contentType: 'application/json',
			dataType: "json"
		})
		.done((data) => {
			$('#item-ID')[0].innerHTML = "";
			uk_alert('uk-alert-primary', 'Deleted item but you want recovery item then click the SAVE button');
		})
		.fail((jqXHR, textStatus) => {
			uk_alert('uk-alert-danger', 'Failure save your data. Check the connection status or try to contact with the server manager.');
		});
	}, function () {
		// PASS
	});
}

// SAVE
function save_data () {
	const data = get_data();

	// Connect API
	$.ajax({
		url: '/manager/item/save',
		method: "POST",
		data: JSON.stringify(data),
		contentType: 'application/json',
		dataType: "json"
	})
	.done((data) => {
		uk_alert('uk-alert-primary', 'Successful save your item data. Edit with confidence!');
	})
	.fail((jqXHR, textStatus) => {
		uk_alert('uk-alert-danger', 'Failure save your data. Check the connection status or try to contact with the server manager.');
	});
}

// GET DATA
function get_data () {
	// Get Item ID (If new item, ID is blink)
	const itemID = $('#item-ID')[0].innerHTML;

	// Get HTML to Markdown Docs
	update_html_docs();

	// Data JSON
	let data = {
		"id": itemID,
		"name": $('#item_name').val(),
		"description": $('#item-description').val(),
		"type": $('#item_type').val(),
		"option": {},
		"paymentMethod": paymentSelector.getSelectedItem().value,
		"payment": {},
		"form": $('#item_form').val(),
		"docs": markdownDocs,
		"extra": {},
		"seat": [],
		"tags": [],
		"public": undefined,
	};

	// Get option
	let prices = $('#option_table tbody tr');
	for (let i=0; i<prices.length; i++) {
		let option = $(prices[i]).children();

		let option_name = option[0].innerHTML;
		let price = option[1].innerHTML;

		// Add price
		data.option[option_name] = price;
	}

	// Get payment infomation
	if (data.paymentMethod == "UserDirectRemit") {
		data.payment = UserDirectRemit;
	}

	// Get extra
	let extra_el = $('#extra_table tbody tr');
	for (let i=0; i<extra_el.length; i++) {
		let el = $(extra_el[i]).children();

		let key = $(el[0]).innerHTML;
		let value = $(el[1]).innerHTML;

		data.extra['key'] = value;
	}

	// Get seat
	let seat_table = $('#seat_table tbody tr');
	for (let i=0; i<seat_table.length; i++) {
		let seat = $(seat_table[i]).children();

		let seat_num = seat[1].innerHTML;
		let seat_pos = seat[2].innerHTML;
		let seat_option = seat[3].innerHTML;

		data.seat.push({
			"num": seat_num,
			"position": seat_pos,
			"option": seat_option
		});
	}

	// Get tags
	let tag_el = $('#item_tags span');
	for (let i=0; i<tag_el.length; i++) {
		let el = tag_el[i].innerHTML;

		data.tags.push(el);
	}

	// Get publish
	let publish_el = $('input[name="publish"]');
	for (let i=0; i<publish_el.length; i++) {
		let el = publish_el[i];
		if (el.checked) {
			data.public = el.value;

			break;
		}
	}

	return data;
}
