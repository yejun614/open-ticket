
// Dropzone
interact('.dropzone').dropzone({
	accept: '.drop',
	overlap: 0.75,

	ondropactivate: (event) => {
		event.target.classList.add('drop-active');
	},
	ondragenter: (event) => {
		var draggableElement = event.relatedTarget;
		var dropzoneElement = event.target;

		dropzoneElement.classList.add('drop-target');
		draggableElement.classList.add('can-drop');
	},
	ondragleave: (event) => {
		event.target.classList.remove('drop-target');
		event.relatedTarget.classList.remove('can-drop');
	},
	ondrop: (event) => {
		$(event.relatedTarget).remove();
	},
	ondropdeactivate: (event) => {
		event.target.classList.remove('drop-active');
		event.target.classList.remove('drop-target');
	}
});

// Drag and drop
interact('.drag-drop').draggable({
	inertia: true,
	modifiers: [
		interact.modifiers.restrictRect({
			restriction: 'parent',
			endOnly: true
		})
	],
	autoScroll: true,
	onmove: dragMoveListener
});

function dragMoveListener (event) {
	var target = event.target;

	var x = (parseFloat(target.getAttribute('data-x')) || 0) + event.dx;
	var y = (parseFloat(target.getAttribute('data-y')) || 0) + event.dy;

	target.style.webkitTransform = 
		target.style.transform = 
			`translate(${x}px, ${y}px)`;

	target.setAttribute('data-x', x);
	target.setAttribute('data-y', y);
}

window.dragMoveListener = dragMoveListener;

// Snapping
interact('.grid-snap').draggable({
	modifiers: [
		interact.modifiers.snap({
			targets: [
				interact.createSnapGrid({ x: 30, y: 30 })
			],
			range: Infinity,
			relativePoints: [ { x: 0, y: 0 } ]
		}),
		interact.modifiers.restrict({
			restriction: 'parent',
			elementRect: { top: 0, left: 0, bottom: 1, right: 1 },
			endOnly: true
		})
	],
	inertia: true,
	onmove: dragMoveListener
});
