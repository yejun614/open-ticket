
function addTerms() {
	const title = $('#terms-form input[name="title"]').val();

	// Clear text input
	$('#terms-form input[name="title"]').val('');

	// Add docs
	add_docs(title, termsCallback);

	// Add to table
	const html = `
	<tr>
		<td>${title}</td>
		<td>
			<button class="uk-button uk-button-primary uk-button-small"
					onclick="open_editor('${title}')">Open</button>
			<button class="uk-button uk-button-danger uk-button-small uk-margin-small-left"
					onclick="if (confirm('Do you really want to delete it?')) {remove_docs('${title}')}">DEL</button>
		</td>
	</tr>`;

	$('#terms-table tbody').append(html);
}

function termsCallback(origin, name) {
	const el = $('#terms-table tbody tr');
	for (let n=0; n<el.length; n++) {
		let tr = $(el[n]).children();

		if (tr[0].innerHTML == origin) {
			if (name == '') {
				// Remove
				$(el[n]).remove();

			} else if (origin != name) {
				// Rename
				tr[0].innerHTML = name;

				$(tr[1]).children()[0].onclick = () => { open_editor(`${name}`) };
				$(tr[1]).children()[1].onclick = () => { remove_docs(`${name}`) };
			}

			break;
		}
	}
}

