
function resize_canvas() {
	const width = $('#canvas-setting input[name="width"]').val();
	const height = $('#canvas-setting input[name="height"]').val();

	$('#seat_canvas').css('width', width + 'px');
	$('#seat_canvas').css('height', height + 'px');
}

function open_canvas() {
	$('#black-background').css('display', 'inline');
	$('#seat_box').css('visibility', 'visible');
}

function close_canvas() {
	// Change Seat Table
	var el = $('#seat_canvas .seat');
	for (var n=0; n<el.length; n++) {
		var target = el[n];

		var x = $(target).attr('data-x');
		var y = $(target).attr('data-y');

		var pos = `(${x}, ${y})`;
		var seat_class = $(target).attr('class').split(/\s+/)[1];

		var table = $($('#seat_table tbody .' + seat_class)[0]).children();
		table[2].innerHTML = pos;
	}

	// Close Canvas
	$('#black-background').css('display', 'none');
	$('#seat_box').css('visibility', 'hidden');
}

// Seat Table Checkbox
$('#seat_table_checkall').change((event) => {
	let checked = event.target.checked;
	let el = $('#seat_table tbody tr');

	for (let n=0; n<el.length; n++) {
		$(el[n]).find('input[type="checkbox"]')[0].checked = checked;
	}
});

// Seat list (Table)
let seat_num_count = 0;
let seat_img_url = 'https://cdn2.iconfinder.com/data/icons/miscellaneous-31/60/box-512.png';

function add_seat(option_num) {
	let option_name = $('#option_table .option_' + option_num + ' td')[0].innerHTML;

	$('#seat_canvas').append(`<div class="seat seat_${seat_num_count} drop grid-snap"><img src="${seat_img_url}"></div>`);
	$('#seat_table tbody').append(`<tr class="seat_${seat_num_count}"><td><input class="uk-checkbox" type="checkbox"></td><td>${seat_num_count}</td><td>(0, 0)</td><td>${option_name}</td></tr>`);

	seat_num_count += 1;
}

function remove_seat () {
	let elements = $('#seat_table tbody tr');

	for (let i=0; i<elements.length; i++) {
		if ( $(elements[i]).find('input[type="checkbox"]')[0].checked ) {
			let className = $(elements[i]).attr('class');
			let el = $('.' + className);

			for (let n=0; n<el.length; n++) {
				$(el[n]).remove();
			}
		}
	}

	$('#seat_table_checkall')[0].checked = false;
}

function remove_seat_once (option_num) {
	let elements = $('.seat_' + option_num);

	for (let i=0; i<elements.length; i++) {
		$(elements[i]).remove();
	}
}

