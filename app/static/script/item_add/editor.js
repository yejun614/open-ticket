
// Init Markdown Editor
const MarkdownEditor = new tui.Editor({
	el: document.querySelector('#markdown-editor'),
	previewStyle: 'vertical',
	height: '100%',
	initialEditType: 'markdown'
});

// Markdown Editor Docs
var markdownDocs = {
	"promotion": {
		"markdown": "",
		"essential": true,
		"rename": false
	}
};

function add_docs(name, callback) {
	markdownDocs[name] = {
		'markdown': '',
		"essential": false,
		'rename': true,
		'callback': callback
	};
}

function remove_docs(name) {
	if (markdownDocs[name].callback != undefined) {
		markdownDocs[name].callback(name, '')
	}

	delete markdownDocs[name];
}

// Markdown Editor
function open_editor(name) {
	var markdown = '';
	if (name in markdownDocs) {
		markdown = markdownDocs[name].markdown;
	} else {
		add_docs(name);
	}

	MarkdownEditor.setMarkdown(markdown);

	$('#markdown_box input[name="title"]').val(name);
	$('#markdown_box input[name="origin-title"]').val(name);

	if (markdownDocs[name].rename == true) {
		$('#markdown_box input[name="title"]').removeAttr('disabled');
	} else {
		$('#markdown_box input[name="title"]').attr('disabled', '');
	}

	$('#black-background').css('display', 'inline');
	$('#markdown_box').css('visibility', 'visible');
}

function close_editor() {
	save_editor();

	$('#black-background').css('display', 'none');
	$('#markdown_box').css('visibility', 'hidden');
}

function save_editor(name) {
	if (name == undefined) {
		name = $('#markdown_box input[name="title"]').val();
	}

	const originTitle = $('#markdown_box input[name="origin-title"]').val();
	const content = MarkdownEditor.getMarkdown();

	if (name != originTitle) {
		// Rename title
		markdownDocs[name] = markdownDocs[originTitle];
		delete markdownDocs[originTitle];
	}

	console.log()
	markdownDocs[name].markdown = content;

	// Save callback function
	const callback = markdownDocs[name].callback;
	if (callback != undefined) {
		callback(originTitle, name);
	}

	// Reopen editor
	open_editor(name);
}

function clear_editor() {
	$('#markdown_box input[name="title"]').val('');
	$('#markdown_box input[name="title"]').removeAttr('disabled');

	MarkdownEditor.setMarkdown('');
}

function fullscreen_editor() {
	const status = $('#markdown_box').hasClass('full-screen');

	if (status) {
		$('#markdown_box').removeClass('full-screen');
		$('#markdown_box .screen-btn')[0].innerHTML = 'Full Screen';
	} else {
		$('#markdown_box').addClass('full-screen');
		$('#markdown_box .screen-btn')[0].innerHTML = 'Normal screen';
	}
}

// Markdown Viewer
function open_viewer(name) {
	// Set data
	$('#markdown_view_box .title')[0].innerHTML = name;

	MarkdownEditor.setMarkdown(markdownDocs[name].markdown);
	$('#markdown-viewer')[0].innerHTML = MarkdownEditor.getHtml();

	// Open viewer
	$('#black-background').css('display', 'inline');
	$('#markdown_view_box').css('visibility', 'visible');
}

function close_view() {
	// Close viewer
	$('#black-background').css('display', 'none');
	$('#markdown_view_box').css('visibility', 'hidden');
}

function fullscreen_viewer() {
	const status = $('#markdown_view_box').hasClass('full-screen');

	if (status) {
		$('#markdown_view_box').removeClass('full-screen');
		$('#markdown_view_box .screen-btn')[0].innerHTML = 'Full Screen';
	} else {
		$('#markdown_view_box').addClass('full-screen');
		$('#markdown_view_box .screen-btn')[0].innerHTML = 'Normal screen';
	}
}

// for SAVE
function update_html_docs() {
	for (key in markdownDocs) {
		MarkdownEditor.setMarkdown(markdownDocs[key].markdown);

		markdownDocs[key].HTML = MarkdownEditor.getHtml();
	}
}
