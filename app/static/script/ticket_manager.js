
function read_ticket_detail(element, data) {
	let html = '';

	// Remove previous labels
	$(element + ' .tags')[0].innerHTML = '';

	// Add label
	if (data.auth) {
		html = `<span class="uk-margin-small-right uk-label">AUTH</span>`
	} else {
		html = `<span class="uk-margin-small-right uk-label uk-label-warning">UNAUTH</span>`;
	}
	$($(element + ' .tags')[0]).append(html);

	if (data.expired) {
		html = `<span class="uk-margin-small-right uk-label uk-label-danger">EXPIRED</span>`;
	} else {
		html = `<span class="uk-margin-small-right uk-label uk-label-success">ACTIVATED</span>`;
	}
	$($(element + ' .tags')[0]).append(html);

	// ID
	$(element + ' input[name="ticket_id"]').val(data.id)

	// Generate barcode
	$(element + ' .barcode').attr('jsbarcode-value', data.id)
	JsBarcode(element + " .barcode").init();

	// Auth
	if (data.auth) {
		$(element + ' span[name="auth"]')[0].innerHTML = 'Auth';
		$($(element + ' span[name="auth"]')[0]).attr('class', 'check uk-text-primary');

		$(element + ' .auth-btn')[0].innerHTML = 'Unauth';
		$($(element + ' .auth-btn')[0]).attr('class', 'auth-btn uk-button uk-button-danger');
	} else {
		$(element + ' span[name="auth"]')[0].innerHTML = 'Unauth';
		$($(element + ' span[name="auth"]')[0]).attr('class', 'check uk-text-danger');

		$(element + ' .auth-btn')[0].innerHTML = 'Auth';
		$($(element + ' .auth-btn')[0]).attr('class', 'auth-btn uk-button uk-button-primary');
	}

	// EMAIL
	$(element + ' input[name="user_email"]').val(data.email)

	// ITEM
	$(element + ' input[name="item_id"]').val(data.item);
	$(element + ' input[name="item_name"]').val(data.item_name);

	// OPTION
	$(element + ' table[name="option-table"] tbody')[0].innerHTML = '';

	option = data.option;
	Object.keys(option, (key) => {
		name = key;
		count = option[key];

		let html = `<tr><td><input class="uk-checkbox" type="checkbox"></td><td>${name}</td><td>${count}</td></tr>`;

		$(element + ' table[name="option-table"] tbody').append(html);
	});

	// SEAT
	$(element + ' table[name="seat-table"] tbody')[0].innerHTML = '';

	seat = data.seat;
	for (let n=0; n<seat.length; n++) {
		num = seat[n].num;
		position = seat[n].position;
		option = seat[n].option;

		let html = `<tr><td><input class="uk-checkbox" type="checkbox"></td><td>${num}</td><td>${position}</td><td>${option}</td></tr>`;

		$(element + ' table[name="seat-table"] tbody').append(html);
	}

	// DATETIME
	$(element + ' input[name="datetime"]').val(data.datetime);
	$(element + ' input[name="expiry_datetime"]').val(data.expiry_datetime);

	// EXPIRED
	if (data.expired) {
		$(element + ' span[name="expired"]')[0].innerHTML = 'EXPIRED';
		$($(element + ' span[name="expired"]')[0]).attr('class', 'check uk-text-danger');

		$(element + ' .expire-btn')[0].innerHTML = 'ACTIVE';
		$($(element + ' .expire-btn')[0]).attr('class', 'expire-btn uk-button uk-button-primary');

	} else {
		$(element + ' span[name="expired"]')[0].innerHTML = 'ACTIVATED';
		$($(element + ' span[name="expired"]')[0]).attr('class', 'check uk-text-primary');

		$(element + ' .expire-btn')[0].innerHTML = 'EXPIRE';
		$($(element + ' .expire-btn')[0]).attr('class', 'expire-btn uk-button uk-button-danger');
	}
}

function ticket_detail(ticket_id) {
	$.ajax({
		url: '/manager/ticket/get',
		method: 'POST',
		data: JSON.stringify({'id': ticket_id}),
		contentType: 'application/json',
		dataType: 'json'
	})
	.done((data) => {
		if (data.success) {
			if ($('#card').css('display') == 'none') {				
				// Change ticket card css
				$('#card').css('visibility', 'hidden');

				// Add html in UIkit modal
				let html = $('#detail')[0].innerHTML;
				$('#modal .detail')[0].innerHTML = html;

				// Read data
				read_ticket_detail('#modal .detail', data.ticket);

				// Show modal
				UIkit.modal('#modal').show();

			} else {
				// Read data
				read_ticket_detail('#detail', data.ticket);

				// Change ticket card css
				$('#card').css('visibility', 'visible');
				
			}
		} else {
			UIkit.notification({
				message: 'System Error! please contact with system manager',
				status: 'danger',
				pos: 'bottom-left',
				timeout: 5000
			});
		}
	})
	.fail((jqXHR, textStatus) => {
		UIkit.notification({
			message: 'System Error! please contact with system manager',
			status: 'danger',
			pos: 'bottom-left',
			timeout: 5000
		});
	});
}

var pageNumber = 0;

function loadNextTickets() {
	$.ajax({
		url: '/manager/ticket/list',
		method: 'POST',
		data: JSON.stringify({
			'num': pageNumber,
			'count': 10
		}),
		contentType: 'application/json',
		dataType: 'json'
	})
	.done((data) => {
		if (data.success) {
			let tickets = data.tickets;

			if (tickets.length <= 0) {
				UIkit.notification({
					message: 'No more tickets',
					pos: 'bottom-left',
					timeout: 5000
				});

				return;
			}

			for (let n=0; n<tickets.length; n++) {
				let ticket = tickets[n];

				let html = `<tr value="${ticket.id}" onclick="ticket_detail('${ticket.id}')">`;

				// auth
				if (ticket.auth) {
					html += `<td><span class="uk-label">AUTH</span></td>`;
				} else {
					html += `<td><span class="uk-label uk-label-warning">UNAUTH</span></td>`;
				}

				// expired
				if (ticket.expired) {
					html += `<td><span class="uk-label uk-label-danger">EXPIRED</span></td>`;
				} else {
					html += `<td><span class="uk-label uk-label-success">ACTIVATED</span></td>`;
				}

				// Email
				html += `<td>${ticket.email}</td>`;

				// Item name
				html += `<td value="${ticket.item}">${ticket.item_name}</td>`;

				// Datetime
				html += `<td>${ticket.datetime}</td>`;

				// Add ticket to table
				$('#table tbody').append(html);
			}

			pageNumber += 1;
		}
	})
	.fail((jqXHR, textStatus) => {
		UIkit.notification({
			message: 'System Error! please contact with system manager',
			status: 'danger',
			pos: 'bottom-left',
			timeout: 5000
		});
	});
}

$(document).ready(() => {
	loadNextTickets();

});
