
var noMoreDataNotification = {
	message: 'No more tickets',
	pos: 'bottom-left',
	timeout: 5000
};

var systemErrorNotification = {
	message: 'System Error! please contact with system manager',
	status: 'danger',
	pos: 'bottom-left',
	timeout: 5000
};

function read_detail(element, data) {
	// ID
	$(element + ' input[name="user_id"]').val(data.id);

	// Username
	$(element + ' input[name="username"]').val(data.username);

	// Level
	$(element + ' input[name="level"]').val(data.level);
}

function get_detail(id) {
	$.ajax({
		url: '/manager/user/get',
		method: 'POST',
		data: JSON.stringify({'id': id}),
		dataType: 'json',
		contentType: 'application/json'
	})
	.done((data) => {
		let user = data.data
		let cardDisplay = $('#card').css('display');

		if (cardDisplay == 'none'){
			$('#card').css('visibility', 'hidden');

			$('#modal .detail')[0].innerHTML = $('#detail')[0].innerHTML;
			read_detail('#modal .detail', user);

			// Show modal
			UIkit.modal('#modal').show();

		} else {
			read_detail('#detail', user);
			$('#card').css('visibility', 'visible');
		}

	})
	.fail(() => { UIkit.notification(systemErrorNotification); });
}

function addUserTable(data) {
	let html = `<tr onclick="get_detail('${data.id}')">`;

	html += `<td>${data.id}</td>`;
	html += `<td>${data.username}</td>`;
	html += `<td>${data.level}</td>`;

	html += '</tr>';

	$('#table tbody').append(html);
}

var pageNumber = 0;
function loadNextData() {
	$.ajax({
		url: '/manager/user/list',
		method: 'POST',
		data: JSON.stringify({
			'num': pageNumber,
			'count': 10
		}),
		dataType: 'json',
		contentType: 'application/json'
	})
	.done((data) => {
		if (data.success) {
			let user = data.data;
			if (user.length <= 0) {
				UIkit.notification(noMoreDataNotification);
			}

			user.forEach((id) => {
				$.ajax({
					url: '/manager/user/get',
					method: 'POST',
					data: JSON.stringify({'id': id}),
					dataType: 'json',
					contentType: 'application/json'
				})
				.done(data => addUserTable(data.data));
			});

			pageNumber += 1;

		} else {
			UIkit.notification(systemErrorNotification);
		}

	})
	.fail(() => { UIkit.notification(systemErrorNotification); });
}

$(document).ready(() => {
	// init
	loadNextData();

	// More data button event
	$('#more-btn').click(() => { loadNextData(); });
});
