
import time
import json
import hashlib

from paylib import database

def getkey(ID):
	keys = [i for i in database.scan_iter('paylog:*:%s*' % ID)]

	if len(keys) > 0:
		return keys[0].decode()
	else:
		return None

def api(data):
	ID = hashlib.sha256( str(time.time()).encode() ).hexdigest()
	key = 'paylog:api:%s' % ID
	value = json.dumps(data)

	database.mset({key:value})

	return ID

def create(method, create_time=None, ticket=None):
	if create_time == None:
		create_time = time.time()

	ID = hashlib.sha256( str(create_time).encode() ).hexdigest()
	key = 'paylog:%s:%s' % (method, ID)

	if ticket != None:
		key += ':%s' % (ticket)

	blink_value = json.dumps({})
	database.mset({key:blink_value})

	return ID

def set_ticket(ID, ticket):
	key = getkey(ID)
	if not key: return False

	info = key.split(':')

	if len(info) < 4:
		info.append(ticket)
	else:
		info[-1] = ticket

	newkey = ''.join(map(lambda s: s+':', info))[:-1]
	database.rename(key, newkey)

	return newkey

def get_ticket(ID):
	key = getkey(ID)
	if not key: return False

	info = key.split(':')

	if len(info) < 4:
		return None
	else:
		return info[-1]

def log(ID, title, content, api_data):
	current_time = time.time()

	data = get(ID)
	if (type(data) != dict): return False

	time_str = time.strftime("%a %d %b %Y %H:%M:%S", time.gmtime(current_time))
	data[time_str] = {
		'title': title,
		'content': content,
		'data': api_data
	}

	key = getkey(ID)
	value = json.dumps(data)
	return database.mset({key:value})

def close(ID, data):
	title = 'CLOSE'
	content = 'Close payment by paylib'
	data['backend_script'] = 'paylib.paylog.close'

	return log(ID, title, content, data)

def get(ID):
	key = getkey(ID)
	if not key: return False

	data = json.loads( database.get(key) )
	return data

def find_by_ticket(ID):
	keys = [i for i in database.scan_iter('paylog:*:%s' % ID)]

	result = []
	for key in keys:
		info = key.decode().split(':')
		result.append(info[2])

	return result

def find_by_method(name):
	keys = [i for i in database.scan_iter('paylog:%s:*' % name)]

	result = []
	for key in keys:
		info = key.decode().split(':')
		result.append(info[2])

	return result
