
import time
strftime = lambda current: time.strftime("%a %d %b %Y %H:%M:%S", time.gmtime(current))

from paylib import database, paylog
from manager import ticket

headers = ['depositor', 'money', 'currency']

def request(data, ticket=None):
	current_time = time.time()
	ID = paylog.create('UserDirectRemit', ticket=ticket)

	log_data = {
		'script': 'paylib.UserDirectRemit.request',
		'datetime': strftime(current_time),
		'data': data
	}
	paylog.log(ID, 'OPEN PAYMENT', 'Open payment with UserDirectRemit', log_data)

	return {'id': ID}

def cancel(ID):
	log_data = {
		'script': 'paylib.UserDirectRemit.cancel',
		'datetime': strftime(time.time())
	}

	# Close ticket (unauth, expired)
	ticketID = paylog.get_ticket(ID)
	if ticketID:
		ticket.permit(database, ticketID, check=False)
		ticket.expire(database, ticketID)

		paylog.log(ID, 'Close Ticket', 'Ticket denied and expired', log_data)

	# Close log
	paylog.close(ID, log_data)

def accept(ID):
	log_data = {
		'script': 'paylib.UserDirectRemit.accept',
		'datetime': strftime(time.time())
	}

	# Get ticket ID
	ticketID = paylog.get_ticket(ID)
	if not ticketID:
		paylog.log(ID, 'No Ticket', 'Can\'t find connected ticket', log_data)

	# Ticket certification
	if ticket.accept(database, ticketID):
		paylog.log(ID, 'Accept Ticket', 'Ticket accepted', log_data)

		# Close log
		paylog.close(ID, log_data)
	else:
		paylog.log(ID, 'No Ticket in DB', 'DB ERROR: Ticket ID (%s) ' % (ticketID), log_data)

page = {
	'form': ''
}

def get_page(name):
	try:
		return page[name]
	except KeyError:
		return 'error.html'
