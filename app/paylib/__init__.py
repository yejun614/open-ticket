
from redis import Redis
database = Redis(host='db', port=6379, db=0)




from . import paylog, \
			  UserDirectRemit
__all__ = [
	'database',
	'paylog',
	'UserDirectRemit'
]

methods = {
	'UserDirectRemit': UserDirectRemit
}
