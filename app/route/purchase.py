from flask import Blueprint, \
				  render_template

from redis import Redis
database = Redis(host='db', port=6379, db=0)

import os
import manager

api = Blueprint('purchase', __name__)

_PURCHASE_FORM_DIR = '/purchase/'

@api.route('/purchase')
@api.route('/purchase/<code>/<item>')
def purchase(code=None, item=None):
	if None in [code, item]:
		return render_template('purchase/error.html', code=code, item=item)

	# Get item
	data = None
	if code == 'id':
		data = manager.item.get_item_id(database, item)
	elif code == 'name':
		data = manager.item.get_item_name(database, item)

	if data is None:
		# Response the error page
		return render_template('purchase/error.html', code=code, item=item, data=None)

	# Get form
	form = _PURCHASE_FORM_DIR
	if ('form' not in data) or (data['form'] == ''):
		form += 'default.html'
	else:
		if os.path.isfile(_PURCHASE_FORM_DIR + data['form']):
			form += data['form']
		else:
			form += 'default.html'

	return render_template(form, code=code, item=item, data=data)
