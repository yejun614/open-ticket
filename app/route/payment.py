from flask import Blueprint, \
				  request, \
				  session, \
				  redirect, \
				  url_for, \
				  render_template

from paylib import paylog, \
				   methods as paylib_methods

from functools import wraps
from route import response

import time
strftime = lambda current: time.strftime("%a %d %b %Y %H:%M:%S", time.gmtime(current))

api = Blueprint('payment', __name__)

def payment_logging(f):
	@wraps(f)
	def decorated_function(*args, **kwargs):
		# Check MINE type
		if not request.is_json:
			return response.response_bad_mimetype()

		# Write log
		data = {}
		data['init_time'] = strftime(time.time())
		data['ip_addr'] = request.remote_addr
		data['conn_api'] = f.__name__
		data['url'] = request.full_path

		paylog.api(data)

		# Return to origin request method
		return f(*args, log=data, **kwargs)

	return decorated_function

def login_required(f):
	@wraps(f)
	def decorated_function(*args, **kwargs):
		if ('user' in session) or ('manager' in session):
			return f(*args, **kwargs)
		else:
			return response.response_sign_in()

	return decorated_function

@api.route('/page', methods=['GET', 'POST'])
@payment_logging
def payment_page(log=None):
	try:
		method = request.json['method']
		page = request.json['page']

		path = paylib_methods[method].get_page(page)

		return render_template('payment/' + path)

	except (ValueError, KeyError, TypeError):
		return response.response_key_error()

@api.route('/request', methods=['POST'])
@payment_logging
def payment_request(log=None):
	result = {}

	try:
		payment = paylib_methods[request.json['method']]
		headers = payment.headers

		data = {}
		for key in headers:
			data[key] = request.json[key]

		ticketID = request.json['ticket']
		result = payment.request(data, ticket=ticketID)

	except (ValueError, KeyError, TypeError):
		return response.response_key_error()

	if ('id' in result):
		return response.response_success_data(result)
	else:
		return response.response_fail_data(result)

@api.route('/cancel', methods=['POST'])
@payment_logging
def payment_cancel(log=None):
	# Get data
	payment = None
	ticketID = ''
	paylogID = ''

	try:
		payment = paylib_methods[request.json['method']]
		ticketID = request.json['ticket']

		paylogID = paylog.find_by_ticket(ticketID)
		if len(paylogID) > 0:
			paylogID = paylogID[0]
		else:
			response.response_fail('Can not find ticket ID')

	except (ValueError, KeyError, TypeError):
		return response.response_key_error()

	# Check user session
	if ('user' in session) and (ticketID in session['user']):
		paylog.log(paylogID, 'User Cancel', 'Request payment cancel by User', log)

	elif 'manager' in session:
		log['manager'] = session['manager']['id']
		paylog.log(paylogID, 'Manager Cancel', 'Request payment cancel by Manager', log)

	else:
		return response.response_sign_in()

	# Payment Cancel
	payment.cancel(paylogID)

	# Return result
	return response.response_success()

@api.route('/getlog', methods=['POST'])
@payment_logging
@login_required
def payment_getlog(log=None):
	try:
		ID = request.json['id']

		result = paylog.get(ID)
		if result:
			return response.response_success_data(result)
		else:
			return response.response_fail('Can not find log ID')

	except (ValueError, KeyError, TypeError):
		return response.response_key_error()

@api.route('/getlog/ticket', methods=['POST'])
@payment_logging
@login_required
def payment_getlog_ticket():
	try:
		# Ticket ID
		ticketID = request.json['ticket']

		# Get paylog ID by ticket
		paylogID = paylog.find_by_ticket(ticketID)
		if len(paylogID) > 0:
			paylogID = paylogID[0]
		else:
			return response.response_fail('Can not find ticket ID')

		# Get paylog
		result = paylog.get(paylogID)

		# Return result
		if result:
			return response.response_success_data(result)
		else:
			return response.response_fail('Can not find log ID')

	except (ValueError, KeyError, TypeError):
		return response.response_key_error()
