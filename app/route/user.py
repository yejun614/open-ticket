from flask import Blueprint, \
				  request, \
				  session, \
				  redirect, \
				  url_for, \
				  render_template

from redis import Redis
import json

import manager
from route.response import *

database = Redis(host='db', port=6379, db=0)

api = Blueprint('user', __name__)

@api.route('/login')
@api.route('/signin')
def login_page():
	if 'user' in session:
		tickets = []
		for ID in session['user']:
			data = manager.ticket.get(database, ID)

			if data:
				data['id'] = ID
				data['item_name'] = manager.item.get_item_id(database, data['item'])['name']

				tickets.append( data )

		return render_template('user.html', \
			tickets=tickets, \
			ticket_len=len(session['user']), \
			email=session['email'])
	else:
		return render_template('user.html')

@api.route('/check')
@api.route('/check/<ID>')
def field_check_page(ID=None):
	if 'user' not in session:
		return redirect(url_for('user.login_page'))

	if ID not in session['user']:
		return render_template('check.html', success=False)

	# Get ticket data
	ticket = manager.ticket.get(database, ID)
	ticket['id'] = ID
	ticket['item_name'] = manager.item.get_item_id(database, ticket['item'])['name']

	# Generate CODE
	check_code = manager.ticket.code(database, ID)

	# Response to check.html
	if (check_code):
		return render_template('check.html', success=True, check_code=check_code, ticket=ticket)
	else:
		return render_template('check.html', success=False)

@api.route('/api/user/login', methods=['POST'])
def login():
	if not request.is_json:
		return response_bad_mimetype()

	email = request.json['email']
	pw = request.json['pw']

	tickets = manager.ticket.user(database, email, pw)

	if not tickets:
		return response_wrong_id()

	if len(tickets) > 0:
		session['user'] = tickets
		session['email'] = email

		return json.dumps({'success':True, 'tickets':tickets}), 200, {'ContentType':'application/json'}
	else:
		return response_wrong_id()

@api.route('/api/user/logout', methods=['GET', 'POST'])
def logout():
	session.pop('user', None)
	return redirect(url_for('index'))
