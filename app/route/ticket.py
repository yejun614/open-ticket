from flask import Blueprint, request, session
from redis import Redis
import json

import manager
from route.response import *

database = Redis(host='db', port=6379, db=0)

api = Blueprint('ticket', __name__)

@api.route('/api/ticket/create', methods=['POST'])
def ticket_create():
	# Check request mimetype
	if not request.is_json:
		return response_bad_mimetype()

	# Manager access only
	if 'manager' not in session:
		return response_only_manager()

	try:
		# Get data, email
		data = request.json;
		email = request.json['email']

		# Set ticket
		result = manager.ticket.set(database, email, data)

		# Check result
		if result:
			return response_success()
		else:
			return response_key_error()

	except (ValueError, KeyError, TypeError):
		return response_key_error()

@api.route('/api/ticket/expire', methods=['POST'])
def ticket_expire():
	# Check request mimetype
	if not request.is_json:
		return response_bad_mimetype()

	# Manager access only
	if 'manager' not in session:
		return response_only_manager()

	try:
		ID = request.json['ID']
		result = manager.ticket.expire(database, ID)

		if result:
			return response_success()
		else:
			return response_id_error()

	except (ValueError, KeyError, TypeError):
		return response_key_error()

@api.route('/api/ticket/update', methods=['POST'])
def ticket_update():
	# Check request mimetype
	if not request.is_json:
		return response_bad_mimetype()

	if ('user' not in session) and ('manager' not in sessino):
		return response_sign_in()

	try:
		ID = request.json['ID']
		data = request.json

		result = manager.ticket.update(database, ID, data)

		if result:
			return response_success()
		else:
			return response_id_error()

	except (ValueError, KeyError, TypeError):
		return response_key_error()

@api.route('/api/ticket/read', methods=['POST'])
def ticket_read():
	# Check request mimetype
	if not request.is_json:
		return response_bad_mimetype()

	if ('user' not in session) and ('manager' not in sessino):
		return response_sign_in()

	try:
		ID = request.json['ID']
		result = manager.ticket.get(database, ID)

		if result:
			return json.dumps({'success':True, 'data':result}), 200, {'ContentType':'application/json'}
		else:
			return response_id_error()

	except (ValueError, KeyError, TypeError):
		return response_key_error()

@api.route('/api/ticket/code', methods=['POST'])
def ticket_gen_code():
	# Check request mimetype
	if not request.is_json:
		return response_bad_mimetype()

	if ('user' not in session) and ('manager' not in sessino):
		return response_sign_in()

	try:
		# Get data
		ID = request.json['ID']
		seconds = 300

		# Check seconds to expire time
		if 'seconds' in request.json:
			seconds = request.json['Seconds']

		# Check Ticket ID and Generate Code
		code = manager.ticket.code(database, ID, seconds)

		if code:
			return json.dumps({'success':True, 'code':code, 'seconds':seconds}), 200, {'ContentType':'application/json'}
		else:
			return response_id_error()
		
	except (ValueError, KeyError, TypeError):
		return response_key_error()


