from flask import Blueprint, \
				  request, \
				  session, \
				  render_template, \
				  redirect, \
				  url_for

import json

from redis import Redis
database = Redis(host='db', port=6379, db=0)

import manager
from paylib import paylog

api = Blueprint('receipt', __name__)

@api.route('/receipt/<ID>', methods=['GET', 'POST'])
def receipt_page(ID=None):
	if 'user' not in session:
		return redirect(url_for('user.login_page'))

	# Get ticket data
	ticket = manager.ticket.get(database, ID)

	if not ticket:
		# Ticket ID Error
		return '''<script>
					alert("Can not found ticket ID\\nPlease contact with system manager");
					window.history.back();
				  </script>'''

	elif ID not in session['user']:
		# Permission Error
		return '''<script>
					alert("You can not access the ticket\\nPlease contact with system manager");
					window.history.back();
				  </script>'''

	# Get paylog
	paylog_data = []
	log_id = paylog.find_by_ticket(ID)

	for ID in log_id:
		data = paylog.get(ID)
		paylog_data.append(data)

	# Return receipt
	return render_template('receipt.html', ticket=ticket, paylog=paylog_data)
