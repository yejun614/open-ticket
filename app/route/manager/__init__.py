from flask import Blueprint, \
				  session, \
				  render_template, \
				  request, \
				  redirect, \
				  url_for

from functools import wraps

import manager
from redis import Redis
database = Redis(host='db', port=6379, db=0)

api = Blueprint('manager', __name__)

def login_required(f):
	@wraps(f)
	def decorated_function(*args, **kwargs):
		if 'manager' not in session:
			return redirect(url_for('manager.manager_auth'))
		return f(*args, **kwargs)

	return decorated_function

@api.route('/')
@login_required
def manager_index():
	return render_template('/manager/index.html')

@api.route('/auth', methods=['GET', 'POST'])
def manager_auth():
	if 'manager' in session:
		return redirect(url_for('manager.manager_index'))
	else:
		return render_template('/manager/login.html')

@api.route('/item')
@login_required
def manager_item():
	items = manager.item.get_items(database)
	return render_template('/manager/item.html', items=items)

@api.route('/payment')
@login_required
def manager_payment():
	pass

@api.route('/setting')
@login_required
def manager_setting():
	pass








from . import item, \
			  auth, \
			  ticket, \
			  user

__all__ = ['api', \
		   'login_required', \
		   'item', \
		   'auth', \
		   'ticket' \
		   'user', \
		   ]
