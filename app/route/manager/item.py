from flask import Blueprint, \
				  request, \
				  session, \
				  redirect, \
				  url_for, \
				  render_template

from route import response
from route.manager import login_required

import manager
from redis import Redis
database = Redis(host='db', port=6379, db=0)

api = Blueprint('manager.item', __name__)

@api.route('/item/edit')
@api.route('/item/edit/<item_id>')
@login_required
def item_edit(item_id=None):
	# New Item
	if item_id == None:
		return render_template('/manager/item_edit.html', item_id='', item={})

	# Get item
	item = manager.item.get_item_id(database, item_id)

	if item:
		# Return item edit
		return render_template('/manager/item_edit.html', \
							   item_id=item_id, \
							   item=item)
	else:
		# Can't find item
		return '<script>alert("%s"); window.history.back();</script>' \
				% ('Can\'t find the item.\\nplease check your item ID.')

@api.route('/item/save', methods=['GET', 'POST'])
@login_required
def item_save():
	if not request.is_json:
		return response.response_bad_mimetype()

	ID = ""
	name = ""
	data = None
	try:
		ID = request.json['id']
		name = response.json['name']
		data = response.json

	except (ValueError, KeyError, TypeError):
		return response.response_key_error()

	result = False

	# Check Item ID
	if ID == "":
		# Create a new item
		result = manager.item.set(database, name, data)
	else:
		# Update item
		result = manager.item.update(database, ID, name, data)

	# Return result
	if result:
		key = manager.item.getkey(database, ID, name).decode()
		itemID = key.split(':')[2]

		return response.response_success_id(itemID)
	else:
		# Failure
		pass


@api.route('/item/remove', methods=['GET', 'POST'])
@login_required
def item_remove():
	if not request.is_json:
		return response.response_bad_mimetype()

	itemID = ''
	try:
		itemID = request.json['id']

	except (ValueError, KeyError, TypeError):
		return response.response_key_error()

	# Remove item
	result = manager.item.delete(database, itemID)

	if result:
		return response.response_success()
	else:
		return response.response_key_error()
