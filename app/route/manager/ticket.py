from flask import Blueprint, \
				  request, \
				  session, \
				  redirect, \
				  url_for, \
				  render_template

from route import response
from route.manager import login_required

import json

import manager
from redis import Redis
database = Redis(host='db', port=6379, db=0)

api = Blueprint('manager.ticket', __name__)

@api.route('/ticket')
@login_required
def managet_ticket():
	return render_template('/manager/ticket.html')

@api.route('/ticket/list', methods=['POST'])
@login_required
def manager_ticket_list():
	if not request.is_json:
		return response.response_bad_mimetype()

	num = None
	count = None

	try:
		num = request.json['num']
		count = request.json['count']
	except (ValueError, KeyError, TypeError):
		return response.response_key_error()

	# Get start and end points
	start = num * count
	end = start + count

	# Get tickets from database
	keys = [i for i in database.scan_iter('*ticket:*')]

	# Check start and end points
	if start < 0:
		start = 0

	length = len(keys)
	if end > length:
		end = length

	# Slice tickets by start to end points
	keys = keys[start:end]

	# Get tickets data
	tickets = []
	for key in keys:
		info = key.decode().split(':')

		data = json.loads( database.get(key) )

		p = 0
		if info[0] == 'unauth':
			data['auth'] = False
			p = 1
		else:
			data['auth'] = True

		data['email'] = info[1+p]
		data['id'] = info[2+p]
		data['item_name'] = manager.item.get_item_id(database, data['item'])['name']

		tickets.append(data)

	# Return tickets
	return json.dumps({'success':True, 'tickets':tickets}), \
		   200, {'ContentType':'application/json'}

@api.route('/ticket/find')
@api.route('/ticket/find/<string:search>')
@login_required
def manager_ticket_find(search=''):
	query_str = request.full_path.split('?')[-1]
	args = query_str.split('=')

	# Filter

@api.route('/ticket/get', methods=['POST'])
@api.route('/ticket/get/<ID>', methods=['GET', 'POST'])
@login_required
def manager_ticket_get(ID=None):
	if not request.is_json:
		return response.response_bad_mimetype()

	try:
		if ID is None:
			ID = request.json['id']

		data = manager.ticket.get(database, ID)

		if data:
			data['item_name'] = manager.item.get_item_id(database, data['item'])['name']

			return json.dumps({'success':True, 'ticket':data}), 200, {'ContentType':'application/json'}

		else:
			return json.dumps({'success':False}), 500, {'ContentType':'application/json'}

	except (ValueError, KeyError, TypeError):
		return response.response_key_error()
