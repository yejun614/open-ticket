from flask import Blueprint, \
				  request, \
				  session, \
				  redirect, \
				  url_for

from redis import Redis
database = Redis(host='db', port=6379, db=0)

from route import response
import manager

api = Blueprint('manager.auth', __name__)

@api.route('/auth/login', methods=['POST'])
def manager_login():
	if not request.is_json:
		return response.response_bad_mimetype()

	try:
		ID = request.json['id']
		password = request.json['password']

		result = manager.user.sign_in(database, ID, password, encrypt=False)

		if result:
			# Add session
			session['manager'] = result

			# Response 200 OK
			return response.response_success()
		else:
			return response.response_wrong_id()

	except (ValueError, KeyError, TypeError):
		return response.response_key_error()

@api.route('/auth/logout', methods=['GET', 'POST'])
def manager_logout():
	if 'manager' in session:
		session.pop('manager', None)
	return redirect(url_for('manager.manager_index'))
