from flask import Blueprint, \
				  render_template, \
				  request

from route import response
from route.manager import login_required

import json

import manager
from redis import Redis
database = Redis(host='db', port=6379, db=0)

api = Blueprint('manager.user', __name__)

@api.route('/user')
@login_required
def manager_user():
	return render_template('/manager/user.html')

@api.route('/user/list', methods=['POST'])
@login_required
def manager_user_list():
	if not request.is_json:
		return response.response_bad_mimetype()

	# Get API parameters from request
	num = 0
	count = 10

	try:
		num = int(request.json['num'])
		count = int(request.json['count'])
	except (ValueError, KeyError, TypeError):
		return response.response_key_error()

	# Set start and end points
	start = num * count
	end = start + count

	# Ready lambda helper function
	get_id = lambda key: key.decode().split(':')[1]

	# Read user ID list from database
	keys = [get_id(i) for i in database.scan_iter('user:*')]
	length = len(keys)

	# Check start and end points
	if start < 0: start = 0
	if end > length: end = length

	# Slice list
	keys = keys[start:end]

	# Return user list
	return response.response_success_data(keys)

@api.route('/user/get', methods=['POST'])
@login_required
def manager_user_get():
	if not request.is_json:
		return response.response_bad_mimetype()

	# Get ID from request json
	ID = ''

	try:
		ID = request.json['id']
	except (ValueError, KeyError, TypeError):
		return response.response_key_error()

	# Read user data from database
	result = manager.user.get(database, ID)

	if result:
		# Remove password in result
		del result['password']

		# Return data
		return response.response_success_data(result)
	else:
		# Return error
		return response.response_wrong_id()
