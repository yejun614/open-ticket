import json

response_success      = lambda: (json.dumps({'success':True})                                                     , 200, {'ContentType':'application/json'})
response_success_id   = lambda data: (json.dumps({'success':True, 'id':data})                                   , 200, {'ContentType':'application/json'})
response_success_data = lambda data: (json.dumps({'success':True, 'data':data})                                   , 200, {'ContentType':'application/json'})

response_fail         = lambda message: (json.dumps({'success':False, 'message':message})                         , 400, {'ContentType':'application/json'})
response_fail_data    = lambda data: (json.dumps({'success':False, 'data':data})                                  , 400, {'ContentType':'application/json'})
response_wrong_id     = lambda: (json.dumps({'success':False, 'message':'Wrong ID, username, email or password'}) , 200, {'ContentType':'application/json'})

response_bad_mimetype = lambda: (json.dumps({'success':False, 'message':'Access by application/json'})            , 400, {'ContentType':'application/json'})
response_key_error    = lambda: (json.dumps({'success':False, 'message':'Check Data keys'})                       , 400, {'ContentType':'application/json'})
response_id_error     = lambda: (json.dumps({'success':False, 'message':'May be can not find ticket ID'})         , 400, {'ContentType':'application/json'})
response_only_manager = lambda: (json.dumps({'success':False, 'message':'Access only manager'})                   , 403, {'ContentType':'application/json'})
response_sign_in      = lambda: (json.dumps({'success':False, 'message':'Sign-in first'})                         , 403, {'ContentType':'application/json'})

response_500          = lambda: (json.dumps({'success':False, 'message':'Internal Server Error'}), 500, {'ContentType':'application/json'})
