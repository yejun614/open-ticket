
import hashlib

# Helper method
def conn_chk(db):
	try:
		return db.ping()
	except:
		return False

# User create
def create(database, ID, username, password, level=1, double_check=True):
	if not conn_chk(database): return False

	if (double_check) and (not check(database, ID)):
		return False

	# Ready key and value(password)
	key = 'user:%s:%s:%d' % (ID, username, level)
	encrypted_pw = hashlib.sha512(password.encode()).hexdigest()

	# Return result
	return database.mset({key:encrypted_pw})

# User remove
def remove(database, ID):
	if not conn_chk(database): return False

	keys = [i for i in database.scan_iter('user:%s:*' % ID)]

	if len(keys) > 0:
		key = keys[0]

		return database.delete(key)
	else:
		return False

# User ID double check
def check(database, ID):
	if not conn_chk(database): return False

	keys = [i for i in database.scan_iter('user:%s:*' % ID)]

	if len(keys) > 0:
		return False
	else:
		return True

# Get user data
def get(database, ID):
	if not conn_chk(database): return False

	keys = [i for i in database.scan_iter('user:%s:*' % ID)]

	if len(keys) <= 0:
		return False

	key = keys[0].decode()
	info = key.split(':')

	# Get data
	data = {}
	data['id'] = info[1]
	data['username'] = info[2]
	data['level'] = info[3]
	data['password'] = database.get(key).decode()

	# Return user data
	return data

# Sign-in
def sign_in(database, ID, password, encrypt=True):
	if not conn_chk(database): return False

	data = get(database, ID)
	db_password = data['password']

	if encrypt:
		password = hashlib.sha512(password.encode()).hexdigest()

	if db_password == password:
		# Remove password data
		del data['password']

		# Return data
		return data
	else:
		return False
