
import os
import json
import time
import hashlib
import random

# Helper method
def conn_chk(db):
	try:
		return db.ping()
	except:
		return False

# Ticket CRUD
def set(database, email, data):
	if not conn_chk(database): return False

	# Generate ID and KEY for Database
	current_time = time.time()

	ID = hashlib.sha256( str(current_time).encode() ).hexdigest()
	key = 'unauth:ticket:%s:%s' % (email, ID)

	# Check data
	checks = ['pw', 'option', 'seat', 'item', 'expiry_datetime']
	for check in checks:
		if check not in data:
			return False

	# Add basic data
	data['datetime'] = time.strftime("%a %d %b %Y %H:%M:%S", time.gmtime(current_time))
	data['expired'] = False

	# to String
	data = json.dumps(data).encode()

	# Add to database
	return database.mset({key:data})

def accept(database, ID):
	if not conn_chk(database): return False

	# Get Ticket key
	keys = [i for i in database.scan_iter('unauth:ticket:*:%s' % ID)]

	# Check the number of keys and rename key
	if len(keys) > 0:
		key = keys[0]
		newkey = key.replace('unauth:', '')

		return database.rename(key, newkey)
	else:
		return False

def update(database, ID, data):
	if not conn_chk(database): return False

	# Get Ticket key
	# Include unauthenticated tickets
	keys = [i for i in database.scan_iter('*ticket:*:%s' % ID)]

	if len(keys) > 0:
		# Get key and data from Database
		key = keys[0]
		db_data = json.loads( database.get(key) )

		# Update data
		for el in data:
			db_data[el] = data[el]

		# to String
		db_data = json.dumps( db_data ).encode()

		# Set data
		return database.mset({key: db_data})

	else:
		# can not find key
		return False

def expire(database, ID):
	# Update expire data
	return update(database, ID, {'expired': True})

def get(database, ID):
	if not conn_chk(database): return False

	# Get Ticket Key
	# Include unauthenticated tickets
	keys = [i for i in database.scan_iter('*ticket:*:%s' % ID)]

	if len(keys) > 0:
		# Get key
		key = keys[0]
		info = key.decode().split(':')

		# Ready data
		data = json.loads( database.get(key) )

		p = 0
		if info[0] == 'unauth':
			data['auth'] = False
			p = 1
		else:
			data['auth'] = True

		data['email'] = info[1+p]
		data['id'] = info[2+p]

		# Returned data
		return data
	else:
		return False

# Field Check
def code(database, ID, seconds=300):
	if not conn_chk(database): return False

	# Get keys
	keys = [i for i in database.scan_iter('ticket:*:%s' % ID)]

	if len(keys) > 0:
		key = keys[0]

		# Generate code and returned it
		return gen_code(database, key, seconds)

	else:
		return False

def gen_code(database, value, seconds, random_chr_len=10):
	if not conn_chk(database): return False

	# Gen key
	current_time = time.time()
	code = '%s-%s' % ( hashlib.sha256( str(current_time).encode() ).hexdigest(), ''.join(map(chr, [random.randint(32, 126) for i in range(10)])) )
	key = 'code:' + code

	# Encode value
	try:
		value = value.encode()
	except AttributeError:
		pass

	# Add code
	result = database.mset({key:value})

	if result:
		# Set expire time to code key
		database.expire(key, seconds)

		# Returned code
		return code
	else:
		return False

def check_code(database, code, remove=False):
	if not conn_chk(database): return False

	# Get keys
	keys = [i for i in database.scan_iter('code:%s' % code)]

	if len(keys) > 0:
		key = keys[0]
		value = database.get(key)

		# Remove code key
		if remove:
			database.delete(key)

		# Returned value of code
		return value
	else:
		return False

def clear_code(database):
	if not conn_chk(database): return False

	# Get keys
	keys = [i for i in database.scan_iter('code:*')]

	for key in keys:
		# remove codes
		database.delete(key)

	return True

# User
def user(database, email, pw):
	if not conn_chk(database): return False

	# Get keys
	# Include unauthenticated tickets
	keys = [i for i in database.scan_iter('*ticket:%s:*' % email)]

	tickets = []
	for key in keys:
		data = json.loads( database.get(key) )

		if data['pw'] == pw:
			tickets.append(key.decode().split(':')[-1])

	return tickets

# Auth
def permit(database, ID, check=True):
	if not conn_chk(database): return False

	keys = [i for i in database.scan_iter('*ticket:*:%s' % ID)]

	if len(keys) <= 0:
		return False

	key = keys[0].decode()
	info = key.split(':')

	result = False

	if (check) and (info[0] == 'unauth'):
		newkey = key[7:]
		result = database.rename(key, newkey)

	elif (not check) and (info[0] == 'ticket'):
		newkey = 'unauth:' + key
		result = database.rename(key, newkey)

	return result
