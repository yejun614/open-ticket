
import time
import json
import hashlib

def get_item_id(database, id):
	try:
		database.ping()
	except ConnectionRefusedError:
		return None

	results = [i for i in database.scan_iter('item:*:%s' % id)]

	item = None
	if len(results) > 0:
		item_key = results[0]
		item = json.loads( database.get(item_key) )

	return item

def get_item_name(database, name):
	try:
		database.ping()
	except ConnectionRefusedError:
		return None

	results = [i for i in database.scan_iter('item:%s:*' % name)]

	item = None
	if len(results) > 0:
		item_key = results[0]
		item = json.loads( database.get(item_key) )

	return item

def get_items(database, check=None):
	try:
		database.ping()
	except ConnectionRefusedError:
		return None

	result = []
	for key in database.scan_iter("item:*"):
		data = json.loads( database.get(key) )

		# filter
		if check:
			for key in check:
				if data[key] != check[key]:
					continue

		result.append(data)

	return result

def set_item(database, name, data):
	try:
		database.ping()
	except ConnectionRefusedError:
		return None

	data = json.dumps(data).encode()
	item_id = hashlib.sha256( str(time.time()).encode() ).hexdigest()

	return database.set('item:%s:%s' % (name, item_id), data)

def conn_chk(db):
	try:
		return db.ping()
	except:
		return False

def getkey(database, ID=None, name=None):
	if not conn_chk(database): return False

	keys = []

	if ID:
		keys += [i for i in database.scan_iter('item:*:%s' % (ID))]
	elif name:
		keys += [i for i in database.scan_iter('item:%s:*' % (name))]

	if len(keys) > 0:
		return keys[0]
	else:
		return None

def get(database, ID):
	key = getkey(database, ID=ID)
	if not key:
		return False

	data = json.loads( database.get(key) )
	return data

def get_name(database, name):
	key = getkey(database, name=name)
	if not key:
		return False

	data = json.loads( database.get(key) )
	return data

def set(database, name, data):
	ID = hashlib.sha256( str(time.time()).encode() ).hexdigest()

	key = 'item:%s:%s' % (name, ID)
	value = json.dumps( data )

	return database.mset({key:value})

def update(database, ID=None, name=None, data=None):
	key = ''
	if ID: key = getkey(database, ID=ID)
	elif name: key = getkey(database, name=name)
	else: return False

	# Update data
	if data:
		value = json.dumps( data )
		database.mset({key:value})

	# Check rename
	info = key.decode().split(':')
	origin_name = info[1]
	if (name != None) and (origin_name != name):
		newkey = 'item:%s:%s' % (name, ID)

		# Rename
		database.rename(key, newkey)

	return True

def delete(database, ID):
	key = getkey(database, ID=ID)

	return database.delete(key)
