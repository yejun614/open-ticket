from flask import Flask, \
				  render_template, \
				  session, \
				  redirect, \
				  url_for, \
				  request

import os

from route import *

app = Flask(__name__)
app.secret_key = os.urandom(100)

# Add blueprint
for route in [purchase, ticket, user]:
	app.register_blueprint(route.api)

# Blueprint of manager
for route in [manager, \
			  manager.auth, \
			  manager.item, \
			  manager.ticket, \
			  manager.user \
			  ]:
			  
	app.register_blueprint(route.api, url_prefix='/manager')

# Payment Blueprint
app.register_blueprint(payment.api, url_prefix='/api/pay')

@app.route('/')
def index():
	return render_template('index.html')
